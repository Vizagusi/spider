package com.demospider;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import org.vaadin.companyspider.Spider;
import org.vaadin.companyspider.data.Department;
import org.vaadin.companyspider.data.Employee;


/**
 * Демо-приложение для демонстрации возможностей Vaadin-компоненты Spider
 * совместимо с @PreserveOnRefresh
 * если пользователь отрефрешит страницу, то сервер в первой выдаче передаст данные ранее выбранной подветки паука
 */

@PreserveOnRefresh
public class DemoSpiderVaadinApplication extends UI {
    Spider spider = null;
    TextField selectedDepartmentNameField = null;
    Label selectedDepartmentSource = null;
    Panel addDepartmentPanel = null;
    Panel addEmployeePanel = null;
    protected int lastUnitId = 0;

    protected Department addDepartment(Department parent, String title) {
        String newId = Integer.toString(++lastUnitId);
        return spider.addDepartment(parent, new Department(newId, title));
    }

    protected Employee addEmployee(Department department, String name, String phone) {
        String newId = Integer.toString(++lastUnitId);
        return spider.addEmployee(department, new Employee(newId, name, phone));
    }

    @Override
    public void init(VaadinRequest request) {
        getPage().setTitle("Демо Vaadin-компоненты Паук связей");

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSizeFull();

        //создание инстанса компоненты
        spider = new Spider();
        spider.setSizeFull();
        //добавление слушателя на событие смены текущего развернутого узла
        spider.addNodeExpandedListener((component, unit, dataSourceType)-> {
            selectedDepartmentNameField.setValue(unit != null ? ((Department) unit).title : "не выбран");
            //тут немного хардкода, но это всего-лишь демо-приложение ;)
            selectedDepartmentSource.setValue("<p style=\"font-size:8px\">данные получены из "+ dataSourceType.name() +"</p>");
            addDepartmentPanel.setEnabled(unit==spider.getRootDepartment());
            addEmployeePanel.setEnabled(unit!=null && unit!=spider.getRootDepartment());
            }
        );

        //добавляем корневой узел и несколько департаментов
        Department rootDepartment = spider.addDepartment(null, new Department("Big big Company"));
        for(int i=0; i<6; i++) {
            Department department = addDepartment(rootDepartment, "Department " + Integer.toString(i+1));
            for(int j=0; j<6; j++) {
                addEmployee(department, "Employee " + Integer.toString(i+1) + "/" + Integer.toString(j+1),
                        Integer.toString(i+1) + "-" + Integer.toString(j+1));
            }
        }

        //панель управления
        Panel controlPanel = new Panel("Паук связей");
        controlPanel.setSizeFull();
        VerticalLayout controlPanelContent = new VerticalLayout();
        controlPanelContent.setSpacing(true);
        controlPanel.setContent(controlPanelContent);

        //панель вывода данных о текущем выбранном узле
        Panel selectedDepartmentPanel = new Panel();
        VerticalLayout selectedDepartmentPanelContent = new VerticalLayout();
        selectedDepartmentPanel.setContent(selectedDepartmentPanelContent);
        selectedDepartmentNameField = new TextField("Выбран департамент: ", "не выбран");
        selectedDepartmentPanelContent.addComponent(selectedDepartmentNameField);
        selectedDepartmentSource = new Label(" ", ContentMode.HTML);
        selectedDepartmentPanelContent.addComponent(selectedDepartmentSource);
        controlPanelContent.addComponent(selectedDepartmentPanel);

        //панель добавления нового департамента
        addDepartmentPanel = new Panel();
        addDepartmentPanel.setEnabled(false);
        VerticalLayout addDepartmentPanelContent = new VerticalLayout();
        addDepartmentPanel.setContent(addDepartmentPanelContent);
        TextField addDepartmentNameField = new TextField("Добавить департамент", "Department");
        addDepartmentPanelContent.addComponent(addDepartmentNameField);
        Button addDepartmentButton = new Button("Добавить");
        addDepartmentButton.addClickListener(event -> {
            addDepartment(spider.getRootDepartment(),
                    addDepartmentNameField.getValue() + " " +
                            Integer.toString(spider.getSubUnits(spider.getRootDepartment()).size() + 1));
        });
        addDepartmentPanelContent.addComponent(addDepartmentButton);
        controlPanelContent.addComponent(addDepartmentPanel);

        //панель добавления нового сотрудника или даже сразу десятерых
        addEmployeePanel = new Panel();
        addEmployeePanel.setEnabled(false);
        VerticalLayout addEmployeePanelContent = new VerticalLayout();
        addEmployeePanel.setContent(addEmployeePanelContent);
        TextField addEmployeeNameField = new TextField("Добавить сотрудника", "Тестов Тест Тестович");
        addEmployeePanelContent.addComponent(addEmployeeNameField);
        TextField addEmployeePhoneField = new TextField("", "+1234567890");
        addEmployeePanelContent.addComponent(addEmployeePhoneField);
        Button addEmployeeButton = new Button("Добавить");
        addEmployeeButton.addClickListener(event -> {
            addEmployee(spider.getExpandedDepartment(),
                    addEmployeeNameField.getValue(),
                    addEmployeePhoneField.getValue());
        });
        addEmployeePanelContent.addComponent(addEmployeeButton);
        Button add10EmployeeButton = new Button("Добавить 10");
        add10EmployeeButton.addClickListener(event -> {
            for(int i=0; i<10; i++) {
                addEmployee(spider.getExpandedDepartment(),
                        addEmployeeNameField.getValue() + Integer.toString(i + 1),
                        addEmployeePhoneField.getValue());
            }
        });
        addEmployeePanelContent.addComponent(add10EmployeeButton);
        controlPanelContent.addComponent(addEmployeePanel);

        layout.addComponent(controlPanel);
        layout.addComponent(spider);
        layout.setExpandRatio(controlPanel, 1);
        layout.setExpandRatio(spider, 4);

        setContent(layout);
    }
}
