/**
 * Коннектор к js-компоненте Паук
 */
org_vaadin_companyspider_Spider =
    function () {
        // Create the component
        var spiderComponent = companyStructure.createSpider(this.getElement());

        //корневой узел иерархии исходных данных
        //кэш, обновляется кусками данными с сервера для текущего развернутого поддерева
        //данные исключаются по списку устаревших с сервера
        this.rootNode = null; //of companyStructure.model.SourceNode

        //карта id -> SourceNode
        this.cachedDataNodes = {};

        //версия кэша
        this.cacheStateVersion = 0;

        //текущий раскрытый узел
        // = null, если выводится только свернутый root
        this.expandedNodeId = null;

        //количество попыток вылечить рассинхронизацию кэша
        this.syncAttempCount = 0;

        var connector = this;

        //обработчик события при запросе загрузки lazy-load веток Паука, транслируется на сервер
        spiderComponent.onNeedToExpandAndGetChilds = function (node) {
            connector.onNeedToExpandAndGetChilds(node.id, false);
            //возвращаем false, т.к. добавление/визуализация будут вызваны асинхронно через onStateChange
            //если дочерние юниты добавляются в кэше синхронно, то надо возвращать true
            return false;
        };
        //обработчик события при разворачивании юнита Паука, транслируется на сервер
        spiderComponent.onNodeExpanded = function (node) {
            connector.expandedNodeId = node?node.id:null;
            connector.onNodeExpanded(node?node.id:"");
        };

        // Handle changes from the server-side
        this.onStateChange = function () {

            //проверка версии кэша
            var cacheVersion = this.getState().cacheStateVersion;
            if (!cacheVersion) cacheVersion = 0;
            var cacheIncremented = (this.cacheStateVersion+1 == cacheVersion);
            //если версии отличаются более чем на единицу, значит рассинхронизация с сервером, надо сбросить кэш
            if (!cacheIncremented && (this.cacheStateVersion != cacheVersion)) {
                this.cachedDataNodes = {};
                this.cacheStateVersion = 0;
                this.rootNode = null;
                if (this.syncAttempCount++ < 5) connector.onNeedToReloadCache();
                else {
                    // если за 5 обращений к серверу не получилось сбросить кэш
                    // TO-DO надо вывести сообщение о фатальной поломке сервера
                }
                return;
            }
            this.cacheStateVersion = cacheVersion;
            this.syncAttempCount = 0;

            if (cacheIncremented) {
                //апдейт кэша по полученным данным
                var cacheUpdater = new this.CacheUpdater(
                    //ссылка на кэш
                    this.cachedDataNodes,
                    //измененные/добавленные узлы, плоский список
                    //формат для отделов: {id, unit="DEPT", parent, hasChilds, title}
                    //формат для сотрудников: {id, unit="EMPL", parent, name, phone}
                    this.getState().diffInBranchToUpdateInCache,
                    //устаревшие, плоский список
                    //формат: массив id
                    //this.getState().obsoleteInCacheIds;
                    this.getState().obsoleteInCacheIds
                );
                cacheUpdater.apply();
                this.rootNode = cacheUpdater.newRootNode ? cacheUpdater.newRootNode : this.rootNode;
                spiderComponent.setRootSourceNode(this.rootNode);
            }

            //визуализация изменений
            var updatesRenderer = new this.UpdatesRenderer(
                //ссылка на компонент
                spiderComponent,
                //ссылка на кэш
                this.cachedDataNodes,
                //корень кэша
                this.rootNode,
                //мап id -> объект изменных узлов
                cacheUpdater?cacheUpdater.changedNodesSet:null,
                //текущий развернутый узел
                this.expandedNodeId,
                //новый развернутый узел
                //формат: long
                this.getState().expandedId
            );
            updatesRenderer.apply();
            this.expandedNodeId = updatesRenderer.getNewExpandedNodeId();
        };

        //класс апдейта локального кэша
        this.CacheUpdater = function(cachedDataNodes, diffInBranchToUpdateInCache, obsoleteInCacheIds) {
            this.cachedDataNodes = cachedDataNodes;
            this.diffInBranchToUpdateInCache = diffInBranchToUpdateInCache;
            this.obsoleteInCacheIds = obsoleteInCacheIds;
            this.newRootNode = null;
            this.changedNodesSet = [];

            this.apply = function() {
                var self = this;
                //обновляем/добавляем
                var updatedSourceNodes = [];
                if (this.diffInBranchToUpdateInCache) this.diffInBranchToUpdateInCache.forEach(function(item) {
                    updatedSourceNodes.push(self.updateSourceNode(item));
                });
                //добавляем ссылки с родительских
                updatedSourceNodes.forEach(function(node) {
                    self.updateParentLinks(node);
                });
                //удаляем устаревшие
                if (this.obsoleteInCacheIds) this.obsoleteInCacheIds.forEach(function(nodeId) {
                    self.removeObsolete(nodeId);
                });
            };

            //формат для отделов: {id, unit="DEPT", parent, hasChilds, title} для отделов
            //формат для сотрудников: {id, unit="EMPL", parent, name, phone} для сотрудников
            this.updateSourceNode = function(item) {
                var sourceNode = this.cachedDataNodes[item.id];
                if (!sourceNode) sourceNode = new companyStructure.model.SourceNode();
                sourceNode.id = item.id;
                sourceNode.parent = item.parent;
                sourceNode.unit = item.unit;
                sourceNode.hasChilds = item.hasChilds;
                if (item.unit=="DEPT") {
                    sourceNode.title =  item.title;
                }
                else if (item.unit=="EMPL") {
                    sourceNode.name = item.name;
                    sourceNode.phone = item.phone;
                }
                this.cachedDataNodes[item.id] = this.changedNodesSet[item.id] = sourceNode;
                return sourceNode;
            };
            //обновление связок юнитов
            this.updateParentLinks = function(node) {
                if (!node.parent) {
                    this.newRootNode = node;
                }
                else {
                    var parent = this.cachedDataNodes[node.parent];
                    if (parent && (parent.subNodes.indexOf(node) == -1)) {
                        parent.subNodes.push(node);
                    }
                }
            };
            //удаление устаревшего юнита из кэша
            this.removeObsolete = function(nodeId) {
                var node = this.cachedDataNodes[nodeId];
                if (!node) return;
                var parent = this.cachedDataNodes[node.parent];
                if (parent) {
                    //удаляем всех и устанавливаем hasChilds, чтобы клиент запросил у сервера загрузку дочерних
                    parent.hasChilds = true; //lazy-load children
                    parent.subNodes = [];
                    this.changedNodesSet[parent.id] = parent;
                }
            };
        };

        //класс рендеринга изменений кэша
        this.UpdatesRenderer = function(spiderComponent, cachedDataNodes, rootNode, changedNodesSet, expandedNodeId, newExpandedNodeId) {
            this.spiderComponent = spiderComponent;
            this.cachedDataNodes = cachedDataNodes;
            this.rootNode = rootNode;
            this.changedNodesSet = changedNodesSet;
            this.expandedNodeId = expandedNodeId;
            this.newExpandedNodeId = newExpandedNodeId;

            this.apply = function() {
                //визуализируем изменения
                //делаем это по возможности с анимацией
                if (this.newExpandedNodeId) {
                    //если нода видима
                    if (this.spiderComponent.isNodeVisible(this.newExpandedNodeId)) {
                        //если нет изменений в ветке выше
                        if (!this.areChangesInPathTo(this.newExpandedNodeId)) {
                            //если выбранная нода не изменилась
                            if (this.newExpandedNodeId == this.expandedNodeId) {
                                //если изменились дочерние, то делаем collapse и expand
                                if (this.areChangesInSub(this.newExpandedNodeId)) {
                                    spiderComponent.collapseNode(this.newExpandedNodeId);
                                    spiderComponent.expandNode(this.newExpandedNodeId);
                                }
                            }
                            //если выбранная нода изменилась, то делаем expand
                            else {
                                this.expandedNodeId = this.newExpandedNodeId;
                                spiderComponent.expandNode(this.newExpandedNodeId);
                            }
                            //ничего не надо менять
                            return;
                        }
                    }
                    //если выбрана невидимая нода или были изменения в ветке ранее выбранной ноды
                    this.expandedNodeId = this.newExpandedNodeId;
                    spiderComponent.rebuildAndUpdate(this.expandedNodeId);
                }
                //если заказано свернуть корень
                else {
                    //и он был развернут и не изменился
                    if ((this.newExpandedNodeId!=this.expandedNodeId)
                        && !this.areChangesInPathTo(this.rootNode)) {
                        //всё сворачиваем
                        spiderComponent.collapseAll();
                    }
                    //он был уже свернут или изменился
                    else {
                        spiderComponent.rebuildAndUpdate(null);
                    }
                    this.expandedNodeId = this.newExpandedNodeId;
                }
            };

            //получить новый развернутый юнит
            this.getNewExpandedNodeId = function() {
                return this.expandedNodeId;
            };

            //Проверить, были ли изменения в ветке выше от заданного юнита
            this.areChangesInPathTo = function(nodeId) {
                if (this.changedNodesSet[nodeId]) return true;
                var node = this.cachedDataNodes[nodeId];
                if (node && node.parent) return this.areChangesInPathTo(node.parent);
                else return false;
            };

            //Проверить, изменились ли дочерние юниты
            this.areChangesInSub = function(nodeId) {
                var node = this.cachedDataNodes[nodeId];
                if (!node) return false;
                var self = this;
                var result = false;
                node.subNodes.forEach(function(subNode) {
                    result = result || self.changedNodesSet[subNode.id]!=null;
                });
                return result;
            }
        };
    };