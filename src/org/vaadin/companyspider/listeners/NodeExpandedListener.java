package org.vaadin.companyspider.listeners;

import org.vaadin.companyspider.Spider;
import org.vaadin.companyspider.data.Unit;

import java.io.Serializable;

/**
 * Слушатель событий разворачивания юнита Паука
 */
public interface NodeExpandedListener extends Serializable {
    /**
     * Источник данных для дочерних юнитов при разворачивании юнита
     * CLIENT_CACHE - данные JS-компонента получила из локального кэша JS-коннектора, без участия сервера
     * SERVER_CONTAINER - данные передал сервер
     */
    enum DataSourceType {
        CLIENT_CACHE,
        SERVER_CONTAINER
    }

    /**
     * Метод обработки события разворачивания юнита Паука
     * @param spider - Vaadin-компонента Паук
     * @param unit - развернутый юнит
     * @param DataSource - тип источника данных для дочерних юнитов
     */
    void nodeExpanded(Spider spider, Unit unit, DataSourceType DataSource);
}