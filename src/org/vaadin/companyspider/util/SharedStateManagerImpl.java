package org.vaadin.companyspider.util;

import org.vaadin.companyspider.data.*;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Реализация менеджера состояния компоненты и обмена этим состоянием с JS-коннектором Паука.
 * Реализовано по принципу репликации изменений клиентского кэша.
 * Сервер "знает", какие юниты содержатся в клиентском кэше.
 * В очередную репликацию сервер включаетт:
 * 1) изменения юнитов, находящихся в ветке Паука, просматриваемой клиентом
 * 2) список идентификаторов измененных юнитов, которые есть в клиентском кэше, но уже не нужны клиенту,
 * т.к. он просматривает другую ветку Паука.
 * 3) номер версии состояния кэша.
 * Клиент накатит репликацию (1) и удалит из кэша (2).
 * Для контроля целостности пакеты изменений помечаются номером версии (3), который увеличивается строго на единицу.
 * Также в состоянии передается идентификатор текущего развернутого юнита.
 */
public class SharedStateManagerImpl implements SharedStateManager {
    //константа - текущий развернутый юнит не определен
    public static final UnitOwner NOT_SPECIFIED = new UnitOwner(null);
    //константа - все юниты свернуты
    public static final UnitOwner COLLAPSED_ROOT = new UnitOwner(null);

    //ссылка на контейнер юнитов
    protected final UnitContainer unitContainer;

    //признак наличия изменений для отправки в клиентский кэш
    protected volatile boolean hasChanges = false;

    //текущий развернутый юнит
    protected UnitOwner expandedUnit = NOT_SPECIFIED;
    //заказанный юнит, который следует развернуть
    protected UnitOwner requestedUnitToExpand = COLLAPSED_ROOT;
    //текущая версия кэша
    protected long cacheStateVersion = 0;
    //набор идентификаторов юнитов, ранее отправленных в клиентский кэш
    protected final Set<String> cachedIdSet = new HashSet<>();
    //набор идентификаторов юнитов, которые следует исключить из клиентского кэша
    protected Set<String> obsoleteInCacheIdSet = new HashSet<>();

    public SharedStateManagerImpl(UnitContainer unitContainer) {
        this.unitContainer = unitContainer;
    }

    /**
     * Пометить юнит измененным. Это нужно, чтобы при последующей подготовке пакета изменений для клиента,
     * менеджер смог эти данные или включить в пакет изменений или добавить в список протухших для удаления из кэша
     * @param unit
     */
    @Override
    public synchronized void markUnitChanged(Unit unit) {
        if (unit==null) return;
        if (cachedIdSet.contains(unit.id)) {
            obsoleteInCacheIdSet.add(unit.id);
            hasChanges = true;
        }
        else if (unit == unitContainer.getRootDepartment()) {
            hasChanges = true;
        }
    }

    /**
     * Обработать запрос от клиентского JS-коннектора на выдачу lazy-loaded данных о дочерних юнитах заданного юнита
     * @param nodeId
     */
    @Override
    public synchronized void processRequestToExpandAndGetChilds(String nodeId) {
        Unit unit = unitContainer.getUnitById(nodeId);
        if (unit instanceof Department) requestedUnitToExpand = new UnitOwner(unit);
        else requestedUnitToExpand = COLLAPSED_ROOT;
        hasChanges = true;
    }

    /**
     * Обработать запрос от клиентского JS-коннектора на сброс серверного представления о состоянии клиентского кэша.
     * Это может потребоваться, когда клиент выявил,
     * что состояние его кэша не соответствует представлению сервера об этом состоянии.
     * Это происходит при рефреше страницы при включенном @PreserveOnRefresh или при каких-либо неполадках
     */
    @Override
    public synchronized void processRequestToReloadCache() {
        cacheStateVersion = 0;
        cachedIdSet.clear();
        obsoleteInCacheIdSet.clear();
        hasChanges = true;
    }

    /**
     * Обработать уведомление от клиентского JS-коннектора о факте разворачивания юнита
     * @param nodeId - идентифкатор развернутого юнита
     */
    @Override
    public synchronized void processEventOnNodeExpanded(String nodeId) {
        Unit unit = unitContainer.getUnitById(nodeId);
        if (unit instanceof Department) expandedUnit = new UnitOwner(unit);
        else if (unit==null) expandedUnit = COLLAPSED_ROOT;
        requestedUnitToExpand = NOT_SPECIFIED;
    }

    /**
     * Имелись ли изменения в состоянии с момента последней отправки клиенту
     * @return - да/нет
     */
    @Override
    public synchronized boolean getHasChanges() {
        return hasChanges;
    }

    /**
     * Метод для обновления sharedState перед его непосредственной отправкой клиенту в JS-коннектор
     * @param state - контейнер состояния
     * @return - обновленный контейнер состояния
     */
    @Override
    public synchronized SpiderState updateState(SpiderState state) {
        if (!hasChanges) return state;
        List<Map<String,String>> diffInBranchToUpdateInCache = new LinkedList<>();
        if (prepareDiff(diffInBranchToUpdateInCache)) {
            state.getDiffInBranchToUpdateInCache().clear();
            state.getDiffInBranchToUpdateInCache().addAll(diffInBranchToUpdateInCache);
            state.getObsoleteInCacheIds().clear();
            state.getObsoleteInCacheIds().addAll(obsoleteInCacheIdSet);
            obsoleteInCacheIdSet.clear();
            state.setCacheStateVersion(++cacheStateVersion);
        }
        state.setExpandedId(expandedUnit.getUnitId());
        return state;
    }

    /**
     * Получить текущий развернутый юнит
     * @return - текущий развернутый юнит
     */
    @Override
    public synchronized Unit getExpandedUnit() {
        return expandedUnit.getUnit();
    }

    /**
     * Получить юнит, который клиент запросил развернуть, но он пока не развернут
     * @return - юнит
     */
    @Override
    public synchronized Unit getRequestedUnitToExpand() {
        return requestedUnitToExpand.getUnit();
    }

    /**
     * Подготовить пакет изменений относительно текущей версии кэша
     * @param diffInBranchToUpdateInCache
     * @return
     */
    protected boolean prepareDiff(List<Map<String,String>> diffInBranchToUpdateInCache) {
        //TO-DO предусмотреть механизм очистки кэша от веток, которые давно не просматривались
        //юниты для обновления в кэше
        Set<Unit> unitsToSend = new HashSet<>();
        //запрошена смена текущего юнита
        if (requestedUnitToExpand!=NOT_SPECIFIED) {
            expandedUnit = requestedUnitToExpand;
            requestedUnitToExpand = NOT_SPECIFIED;
        }
        //выбираем из ветки те юниты, которые еще не в кеше или уже устарели в нем
        if (expandedUnit==COLLAPSED_ROOT) {
            Department department = unitContainer.getRootDepartment();
            if (!cachedIdSet.contains(department.id) || obsoleteInCacheIdSet.contains(department.id))
                unitsToSend.add(department);
        }
        else if (expandedUnit!=NOT_SPECIFIED) {
            Department department = (Department)expandedUnit.unit;
            while (department != null) {
                if (!cachedIdSet.contains(department.id) || obsoleteInCacheIdSet.contains(department.id))
                    unitsToSend.add(department);
                unitsToSend.addAll(unitContainer.getSubUnits(department)
                        .stream()
                        .filter(subUnit -> !cachedIdSet.contains(subUnit.id) || obsoleteInCacheIdSet.contains(subUnit.id))
                        .collect(Collectors.toList()));
                department = unitContainer.getParentDepartment(department);
            }
        }
        //формирование простыни данных
        diffInBranchToUpdateInCache.clear();
        for(Unit unit : unitsToSend) {
            //регистрируем, как отданное в кэш клиента
            cachedIdSet.add(unit.id);
            //если есть в obsolete, то исключаем из obsolete
            obsoleteInCacheIdSet.remove(unit.id);
            //добавляем key-value в список для отправки в кэш
            diffInBranchToUpdateInCache.add(translateUnitToMap(unit));
        }
        //убираем из списка ранее закэшированных те, которые помечены к удалению
        cachedIdSet.removeAll(obsoleteInCacheIdSet);
        //возвращаем признак наличия подготовленных изменений кэша
        return !unitsToSend.isEmpty() || !obsoleteInCacheIdSet.isEmpty();
    }

    // ---------- преобразователи из объектов модели в key-value для передачи JS-коннектору
    //TO-DO: если модель будет разрастаться, лучше подключить com.google.gson.Gson для генерации Json через Reflection

    public static final String JS_FIELD_ID         = "id";         //идентификатор
    public static final String JS_FIELD_PARENT     = "parent";     //идентификатор родителя
    public static final String JS_FIELD_UNIT       = "unit";       //"DEPT" - отдел, "EMPL" - работник
    public static final String JS_FIELD_HAS_CHILDS = "hasChilds";  //признак наличия дочерних юнитов, ="1" for lazy loading
    public static final String JS_FIELD_TITLE      = "title";      //название компании
    public static final String JS_FIELD_NAME       = "name";       //ФИО сотрудника
    public static final String JS_FIELD_PHONE      = "phone";      //телефон сотрудника
    public static final String JS_UNIT_DEPARTMENT  = "DEPT";       //тип юнита для отдела
    public static final String JS_UNIT_EMPLOYEE    = "EMPL";       //тип юнита сотрудника

    /**
     * Преобразовать юнит в key-value для передачи JS-коннектору
     * TO-DO: если модель будет разрастаться, лучше подключить com.google.gson.Gson
     * @param unit - юнит для трансформации в key-value
     * @return готовый key-value
     */
    protected Map<String,String> translateUnitToMap(Unit unit) {
        if (unit instanceof Department) return translateDepartmentToMap((Department)unit);
        else if (unit instanceof Employee) return translateEmployeeToMap((Employee)unit);
        else throw new IllegalArgumentException("Unknown unit type.");
    }

    /**
     * Преобразовать объект "отдел" в key-value для передачи JS-коннектору
     * TO-DO: если модель будет разрастаться, лучше подключить com.google.gson.Gson
     * @param department - отдел для трансформации в key-value
     * @return готовый key-value
     */
    protected Map<String,String> translateDepartmentToMap(Department department) {
        Department parent = unitContainer.getParentDepartment(department);
        Map<String,String> to = new TreeMap<>();
        to.put(JS_FIELD_ID,         department.id);
        to.put(JS_FIELD_PARENT,     parent!=null?parent.id:"");
        to.put(JS_FIELD_HAS_CHILDS, unitContainer.hasSubUnits(department)?"1":"");
        to.put(JS_FIELD_UNIT,       JS_UNIT_DEPARTMENT);
        to.put(JS_FIELD_TITLE,      department.title);
        return to;
    }

    /**
     * Преобразовать объект "работник" в key-value для передачи JS-коннектору
     * TO-DO: если модель будет разрастаться, лучше подключить com.google.gson.Gson
     * @param employee - работник для трансформации в key-value
     * @return готовый key-value
     */
    protected Map<String,String> translateEmployeeToMap(Employee employee) {
        Department parent = unitContainer.getParentDepartment(employee);
        Map<String,String> to = new TreeMap<>();
        to.put(JS_FIELD_ID,     employee.id);
        to.put(JS_FIELD_PARENT, parent!=null?parent.id:"");
        to.put(JS_FIELD_UNIT,   JS_UNIT_EMPLOYEE);
        to.put(JS_FIELD_NAME,   employee.name);
        to.put(JS_FIELD_PHONE , employee.phone);
        return to;
    }
}
