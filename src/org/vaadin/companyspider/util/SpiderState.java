package org.vaadin.companyspider.util;

import com.vaadin.shared.ui.JavaScriptComponentState;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Состояние Паука
 */
public class SpiderState extends JavaScriptComponentState {

    //версия кеша, инкрементируется каждый раз при изменении obsoleteInCacheIdSet и diffInBranchToUpdateInCache
    //проверяется на клиенте для обнаружения состояния неконсистентности кеша
    //это нужно, в частности, при режиме @PreserveOnRefresh
    protected long cacheStateVersion = 0;

    //список идентификаторов измененных узлов, которые следует удалить из кеша
    protected List<String> obsoleteInCacheIds = new LinkedList<>();

    //узлы в просматриваемой ветке, которые или устарели в кеше, или еще не были загружены
    //формат для отделов: {id, parent, unit="DEPT", title} для отделов
    //формат для сотрудников: {id, parent, unit="EMPL", name, phone} для сотрудников
    //передаем плоской простыней, а не деревом, чтобы не включать промежуточные неизмененные узлы
    protected List<Map<String,String>> diffInBranchToUpdateInCache = new LinkedList<>();

    //идентификатор текущего развернутого узла, равен null если все свернуты
    protected String expandedId = null;

    //----- геттеры и сеттеры -----
    public List<String> getObsoleteInCacheIds() {
        return obsoleteInCacheIds;
    }

    public void setObsoleteInCacheIds(List<String> obsoleteInCacheIds) {
        this.obsoleteInCacheIds = obsoleteInCacheIds;
    }

    public List<Map<String, String>> getDiffInBranchToUpdateInCache() {
        return diffInBranchToUpdateInCache;
    }

    public void setDiffInBranchToUpdateInCache(List<Map<String, String>> diffInBranchToUpdateInCache) {
        this.diffInBranchToUpdateInCache = diffInBranchToUpdateInCache;
    }

    public String getExpandedId() {
        return expandedId;
    }

    public void setExpandedId(String expandedId) {
        this.expandedId = expandedId;
    }

    public long getCacheStateVersion() {
        return cacheStateVersion;
    }

    public void setCacheStateVersion(long value) {
        this.cacheStateVersion = value;
    }
}
