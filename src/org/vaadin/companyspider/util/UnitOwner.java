package org.vaadin.companyspider.util;

import org.vaadin.companyspider.data.Unit;

/**
 * Контейнер одного юнита. Используется для расширения множества юнитов предопределенными системными значениями,
 * вроде "ни один из юнитов не выбран", "юнит не задан"
 */
public class UnitOwner {
    protected Unit unit = null;

    public UnitOwner(Unit unit) {
        this.unit = unit;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getUnitId() {
        return unit!=null?unit.id:null;
    }

    public boolean UnitsAreEqual(UnitOwner another) {
        return (unit==null && (another==null || another.unit==null)) ||
                (unit!=null && another!=null && this.unit.equals(another.unit));
    }
}
