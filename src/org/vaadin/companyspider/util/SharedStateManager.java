package org.vaadin.companyspider.util;

import org.vaadin.companyspider.data.Unit;

/**
 * Интерфейс менеджера состояния компоненты и обмена этим состоянием с JS-коннектором Паука
 */
public interface SharedStateManager {
    /**
     * Метод для обновления sharedState перед его непосредственной отправкой клиенту в JS-коннектор
     * @param state - контейнер состояния
     * @return - обновленный контейнер состояния
     */
    SpiderState updateState(SpiderState state);

    /**
     * Имелись ли изменения в состоянии с момента последней отправки клиенту
     * @return - да/нет
     */
    boolean getHasChanges();

    /**
     * Пометить юнит измененным. Это нужно, чтобы при последующей подготовке пакета изменений для клиента,
     * менеджер смог эти данные или включить в пакет изменений или добавить в список протухших для удаления из кэша
     * @param unit
     */
    void markUnitChanged(Unit unit);

    /**
     * Обработать запрос от клиентского JS-коннектора на выдачу lazy-loaded данных о дочерних юнитах заданного юнита
     * @param nodeId
     */
    void processRequestToExpandAndGetChilds(String nodeId);

    /**
     * Обработать запрос от клиентского JS-коннектора на сброс серверного представления о состоянии клиентского кэша.
     * Это может потребоваться, когда клиент выявил,
     * что состояние его кэша не соответствует представлению сервера об этом состоянии.
     * Это происходит при рефреше страницы при включенном @PreserveOnRefresh или при каких-либо неполадках
     */
    void processRequestToReloadCache();

    /**
     * Обработать уведомление от клиентского JS-коннектора о факте разворачивания юнита
     * @param nodeId - идентифкатор развернутого юнита
     */
    void processEventOnNodeExpanded(String nodeId);

    /**
     * Получить текущий развернутый юнит
     * @return - текущий развернутый юнит
     */
    Unit getExpandedUnit();

    /**
     * Получить юнит, который клиент запросил развернуть, но он пока не развернут
     * @return - юнит
     */
    Unit getRequestedUnitToExpand();
}

