package org.vaadin.companyspider;

import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.StyleSheet;
import com.vaadin.data.Container;
import com.vaadin.event.SelectionEvent;
import com.vaadin.ui.AbstractJavaScriptComponent;
import com.vaadin.ui.Component;
import elemental.json.JsonArray;
import elemental.json.JsonException;
import org.vaadin.companyspider.data.*;
import org.vaadin.companyspider.data.UnitContainer;
import org.vaadin.companyspider.listeners.NodeExpandedListener;
import org.vaadin.companyspider.util.SharedStateManager;
import org.vaadin.companyspider.util.SharedStateManagerImpl;
import org.vaadin.companyspider.util.SpiderState;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;


/**
 * Vaadin-компонента, являющаяся оберткой к JS-компоненте Паук
 */
@JavaScript({"company-struct.js", "spider-connector.js", "http://d3js.org/d3.v3.min.js"})
@StyleSheet({"company-struct.css"})
public class Spider extends AbstractJavaScriptComponent implements UnitContainer {
    //контейнер юнитов
    protected UnitContainer items;
    //менеджер состояния
    protected SharedStateManager sharedStateManager;
    //коллекция слушателей события разворчивания юнита
    private Collection<NodeExpandedListener> nodeExpandedListeners = new LinkedList<>();

    /**
     * Конструктор по умолчанию
     * Содастся с контейнером по умолчанию
     */
    public Spider() {
        this(new UnitContainerImpl());
    }

    /**
     * Конструктор для создания Паука по заданному источнику данных
     * @param dataSource - заданный источник данных
     */
    public Spider(Container.Hierarchical dataSource) {
        this(new UnitContainerImpl(dataSource));
    }

    public Spider(UnitContainer unitContainer) {
        items = unitContainer;
        sharedStateManager = new SharedStateManagerImpl(unitContainer);
        addFunction("onNeedToExpandAndGetChilds", this::onNeedToExpandAndGetChilds);
        addFunction("onNeedToReloadCache", this::onNeedToReloadCache);
        addFunction("onNodeExpanded", this::onNodeExpanded);
    }

    /**
     * Обработчик запрос от клиентского JS-коннектора на выдачу lazy-loaded данных о дочерних юнитах заданного юнита
     * @param arguments - идентификатор юнита
     * @throws JsonException
     */
    protected void onNeedToExpandAndGetChilds(JsonArray arguments) throws JsonException {
        sharedStateManager.processRequestToExpandAndGetChilds(arguments.getString(0));
        markAsDirty();
        //уведомляем слушателей, т.к. onNodeExpanded не будет в целях уменьшения трафика
        for (NodeExpandedListener listener : nodeExpandedListeners)
            listener.nodeExpanded(this, sharedStateManager.getRequestedUnitToExpand(),
                    NodeExpandedListener.DataSourceType.SERVER_CONTAINER);
    }

    /**
     * Обработчик запроса от клиентского JS-коннектора на сброс серверного представления о состоянии клиентского кэша.
     * Это может потребоваться, когда клиент выявил,
     * что состояние его кэша не соответствует представлению сервера об этом состоянии.
     * Это происходит при рефреше страницы при включенном @PreserveOnRefresh или при каких-либо неполадках
     * @param arguments - пустой
     * @throws JsonException
     */
    protected void onNeedToReloadCache(JsonArray arguments) throws JsonException {
        sharedStateManager.processRequestToReloadCache();
        markAsDirty();
    }

    /**
     * Обработчик уведомления от клиентского JS-коннектора о факте разворачивания юнита
     * @param arguments - идентифкатор развернутого юнита
     */
    protected void onNodeExpanded(JsonArray arguments) throws JsonException {
        sharedStateManager.processEventOnNodeExpanded(arguments.getString(0));
        for (NodeExpandedListener listener : nodeExpandedListeners)
            listener.nodeExpanded(this, sharedStateManager.getExpandedUnit(),
                    NodeExpandedListener.DataSourceType.CLIENT_CACHE);
    }

    /**
     * Получить текущий развернутый департамент
     * @return - текущий развернутый департамент
     */
    public Department getExpandedDepartment() {
        return (sharedStateManager.getExpandedUnit() instanceof  Department)?
                (Department)sharedStateManager.getExpandedUnit(): null;
    }

    /**
     * Приведение типа состояния компоненты
     * @return
     */
    @Override
    protected SpiderState getState() {
        return (SpiderState)super.getState();
    }

    /**
     * Репликацию с изменениями кладем в общее состояние только непосредственно перед его отправкой клиенту
     * @param initial
     */
    @Override
    public void beforeClientResponse(boolean initial) {
        super.beforeClientResponse(initial);
        if (sharedStateManager.getHasChanges()) {
            sharedStateManager.updateState(getState());
        }
    }

    /**
     * Получить корневой юнит
     * @return - корневой юнит (организация в целом)
     */
    @Override
    public Department getRootDepartment() {
        return items.getRootDepartment();
    }

    /**
     * Получить департамент, являющийся родительским для заданного юнита
     * @param unit - юнит
     * @return - департамент юнита
     */
    @Override
    public Department getParentDepartment(org.vaadin.companyspider.data.Unit unit) {
        return items.getParentDepartment(unit);
    }

    /**
     * Получить юнита по его идентификатору
     * @param id - идентификатор
     * @return - юнит
     */
    @Override
    public org.vaadin.companyspider.data.Unit getUnitById(String id) {
        return items.getUnitById(id);
    }

    /**
     * Добавить новый департамент в Паук
     * @param parent - родительский департамент. Равен null для добавления корня
     * @param department - добавляемый департамент
     * @return - добавляемый департамент
     */
    @Override
    public Department addDepartment(Department parent, Department department) {
        Department result = items.addDepartment(parent, department);
        sharedStateManager.markUnitChanged(result);
        markAsDirty();
        return result;
    }

    /**
     * Исключить заданный департамент из Паука
     * @param department - департамент
     * @return - успех/неуспех
     */
    @Override
    public boolean removeDepartment(Department department) {
        throw new UnsupportedOperationException("Method is not implemented yet.");
    }

    /**
     * Добавить нового работника в Паук
     * @param department - родительский департамент
     * @param employee - работник
     * @return - работник
     */
    @Override
    public Employee addEmployee(Department department, Employee employee) {
        Employee result = items.addEmployee(department, employee);
        sharedStateManager.markUnitChanged(result);
        markAsDirty();
        return result;
    }

    /**
     * Исключить работника из Паука
     * @param department - из какого департамента
     * @param employee - работник
     * @return - успех/неуспех
     */
    @Override
    public boolean removeEmployee(Department department, Employee employee) {
        throw new UnsupportedOperationException("Method is not implemented yet.");
    }

    /**
     * Найти департаменты по названию
     * @param title - название
     * @return - список департаментов
     */
    @Override
    public Collection<Department> searchDepartmentsByTitle(String title) {
        return items.searchDepartmentsByTitle(title);
    }

    /**
     * Получить юниты заданного класса под заданным департаментом
     * @param department - департамент
     * @param type - класс юнитов
     * @param <T> - один из наследников класса юнитов
     * @return - коллекция юнитов
     */
    @Override
    public <T extends org.vaadin.companyspider.data.Unit> Collection<T> getSubUnits(Department department, Class<T> type) {
        return items.getSubUnits(department, type);
    }

    /**
     * Получить юниты любых классов под заданным департаментом
     * @param department - департамент
     * @return - коллекция юнитов
     */
    @Override
    public Collection<org.vaadin.companyspider.data.Unit> getSubUnits(Department department) {
        return items.getSubUnits(department);
    }

    /**
     * Имеет ли заданный департамент дочерние юниты
     * @param department - департамент
     * @return - имеет/не имеет
     */
    @Override
    public boolean hasSubUnits(Department department) {
        return items.hasSubUnits(department);
    }

    /**
     * Добавить слушателя событий разворачивания юнита
     * @param listener - слушатель
     * @return - неуспех, если был добавлен ранее
     */
    public boolean addNodeExpandedListener(NodeExpandedListener listener) {
        if (!nodeExpandedListeners.contains(listener)) {
            return nodeExpandedListeners.add(listener);
        }
        return true;
    }

    /**
     * Удалить слушателя
     * @param listener - слушатель
     * @return - неуспех, если слушателя не было
     */
    public boolean removeNodeExpandedListener(NodeExpandedListener listener) {
        return nodeExpandedListeners.remove(listener);
    }

}
