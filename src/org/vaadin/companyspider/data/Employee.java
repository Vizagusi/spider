package org.vaadin.companyspider.data;

import java.io.Serializable;

/**
 * Работник - объект модели данных презентационного уровня
 */
public class Employee extends Unit implements Serializable {
    public String name;
    public String phone;

    /**
     * Создать работника с заданным ФИО и телефоном. В качестве идентификатора будет использоваться ФИО.
     * @param name - ФИО
     * @param phone - телефон
     */
    public Employee(String name, String phone) {
        this(name, name, phone);
    }

    /**
     * Создать работника с заданным идентификатором, ФИО и телефоном
     * @param id - идентификатор
     * @param name - ФИО
     * @param phone - телефон
     */
    public Employee(String id, String name, String phone) {
        super(id);
        this.name = name;
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id.equals(employee.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
