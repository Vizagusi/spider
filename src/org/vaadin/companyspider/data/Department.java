package org.vaadin.companyspider.data;

import java.io.Serializable;

/**
 * Департамент - объект модели данных презентационного уровня
 */
public class Department extends Unit implements Serializable {
    public String title;

    /**
     * Создать департамент с заданным названием. В качестве идентификатора будет использоваться название.
     * @param title - название департамента
     */
    public Department(String title) {
        this(title, title);
    }

    /**
     * Создать департамент с заданным идентификатором и названием
     * @param id
     * @param title
     */
    public Department(String id, String title) {
        super(id);
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
