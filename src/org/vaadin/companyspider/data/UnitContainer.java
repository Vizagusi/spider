package org.vaadin.companyspider.data;

import java.io.Serializable;
import java.util.Collection;

/**
 * Интерфейс контейнера данных презентационного уровня Паука
 */
public interface UnitContainer extends Serializable {
    /**
     * Получить корневой юнит
     * @return - корневой юнит (организация в целом)
     */
    Department getRootDepartment();

    /**
     * Получить департамент, являющийся родительским для заданного юнита
     * @param unit - юнит
     * @return - департамент юнита
     */
    Department getParentDepartment(Unit unit);

    /**
     * Получить юнита по его идентификатору
     * @param id - идентификатор
     * @return - юнит
     */
    Unit getUnitById(String id);

    /**
     * Добавить новый департамент в контейнер
     * @param parent - родительский департамент. Равен null для добавления корня
     * @param department - добавляемый департамент
     * @return - добавляемый департамент
     */
    Department addDepartment(Department parent, Department department);

    /**
     * Исключить заданный департамент из контейнера
     * @param department - департамент
     * @return - успех/неуспех
     */
    boolean removeDepartment(Department department);

    /**
     * Добавить нового работника в контейнер
     * @param department - родительский департамент
     * @param employee - работник
     * @return - работник
     */
    Employee addEmployee(Department department, Employee employee);

    /**
     * Исключить работника из контейнера
     * @param department - из какого департамента
     * @param employee - работник
     * @return - успех/неуспех
     */
    boolean removeEmployee(Department department, Employee employee);

    /**
     * Найти департаменты по названию
     * @param title - название
     * @return - список департаментов
     */
    Collection<Department> searchDepartmentsByTitle(String title);

    /**
     * Получить юниты заданного класса под заданным департаментом
     * @param department - департамент
     * @param type - класс юнитов
     * @param <T> - один из наследников класса юнитов
     * @return - коллекция юнитов
     */
    <T extends Unit> Collection<T> getSubUnits(Department department, Class<T> type);

    /**
     * Получить юниты любых классов под заданным департаментом
     * @param department - департамент
     * @return - коллекция юнитов
     */
    Collection<Unit> getSubUnits(Department department);

    /**
     * Имеет ли заданный департамент дочерние юниты
     * @param department - департамент
     * @return - имеет/не имеет
     */
    boolean hasSubUnits(Department department);
}
