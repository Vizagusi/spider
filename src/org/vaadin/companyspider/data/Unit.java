package org.vaadin.companyspider.data;

/**
 * Единица орг.структуры - базовый объект модели данных презентационного уровня
 */
public abstract class Unit  {
    public String id;

    /**
     * Создать единицу орг.структуры с заданным идентификатором
     * @param id - идентификатор
     */
    public Unit(String id) {
        this.id = id;
    }
}
