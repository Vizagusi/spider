package org.vaadin.companyspider.data;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.HierarchicalContainer;

import java.util.*;

/**
 * Реализация контейнера юнитов орг.структуры на базе Container.Hierarchical
 */
public class UnitContainerImpl implements UnitContainer {
    protected final static String keyPropertyData = "UNIT_OBJ";
    protected final static String keyPropertyId = "ID";
    protected final static String keyPropertyTitle = "TITLE";
    protected final static String keyPropertyName = "NAME";
    protected final static String keyPropertyPhone = "PHONE";
    protected final boolean readOnlyMode;
    protected Container.Hierarchical items;

    /**
     * Конструктор по умолчанию. Будет создан пустой контейнер
     */
    public UnitContainerImpl() {
        items = new HierarchicalContainer();
        items.addContainerProperty(keyPropertyData, Unit.class, null);
        readOnlyMode = false;
    }

    /**
     * Конструктор с заданным готовым контейнером данных на входе.
     * В таком режиме наш контейнер будет транслировать из карты свойств в объекты нашей модели
     * @param dataSource - источник данных
     */
    public UnitContainerImpl(Container.Hierarchical dataSource) {
        items = dataSource;
        readOnlyMode = !items.getContainerPropertyIds().contains(keyPropertyData);
    }

    /**
     * Получить юнит по данным элемента Container.Hierarchical
     * @param item - элемент Container.Hierarchical
     * @return - юнит
     */
    protected Unit getUnitFromItem(Item item) {
        Property propertyData = item.getItemProperty(keyPropertyData);
        if (propertyData != null) {
            if (propertyData.getValue() instanceof Unit)
                return (Unit) propertyData.getValue();
            else
                throw new IllegalArgumentException("Given item has another type.");
        } else {
            Property propertyTitle = item.getItemProperty(keyPropertyTitle);
            Property propertyName = item.getItemProperty(keyPropertyName);
            Property propertyId = item.getItemProperty(keyPropertyId);
            Property propertyPhone = item.getItemProperty(keyPropertyPhone);
            if (propertyTitle != null && propertyTitle.getValue() != null) {
                if (propertyId == null) propertyId = propertyTitle;
                return new Department(
                        propertyId.getValue().toString(),
                        propertyTitle.getValue().toString()
                );
            } else if (propertyName != null && propertyName.getValue() != null) {
                if (propertyId == null) propertyId = propertyName;
                return new Employee(
                        propertyId.getValue().toString(),
                        propertyName.getValue().toString(),
                        (propertyPhone != null && propertyPhone.getValue() != null) ? propertyPhone.getValue().toString() : ""
                );
            } else throw new IllegalArgumentException("Wrong properties.");
        }
    }

    /**
     * Получить департамент по данным элемента Container.Hierarchical
     * @param item - элемент Container.Hierarchical
     * @return - департамент
     */
    protected Department getDepartmentFromItem(Item item) {
        Unit unit = getUnitFromItem(item);
        if (unit instanceof Department) return (Department) unit;
        else throw new IllegalArgumentException("Given item has another type.");
    }

    /**
     * Получить работника по данным элемента Container.Hierarchical
     * @param item - элемент Container.Hierarchical
     * @return - работник
     */
    protected Employee getEmployeeFromItem(Item item) {
        Unit unit = getUnitFromItem(item);
        if (unit instanceof Employee) return (Employee) unit;
        else throw new IllegalArgumentException("Given item has another type.");
    }

    /**
     * Получить корневой юнит
     * @return - корневой юнит (организация в целом)
     */
    @Override
    public Department getRootDepartment() {
        Iterator<?> i = items.rootItemIds().iterator();
        if (i.hasNext()) return getDepartmentFromItem(items.getItem(i.next()));
        else return null;
    }

    /**
     * Получить департамент, являющийся родительским для заданного юнита
     * @param unit - юнит
     * @return - департамент юнита
     */
    @Override
    public Department getParentDepartment(Unit unit) {
        Object parentItemKey = items.getParent(unit.id);
        if (parentItemKey!=null) return getDepartmentFromItem(items.getItem(parentItemKey));
        else return null;
    }

    /**
     * Получить юнита по его идентификатору
     * @param id - идентификатор
     * @return - юнит
     */
    @Override
    public Unit getUnitById(String id) {
        if (id==null) return null;
        Item item = items.getItem(id);
        if (item!=null) return getUnitFromItem(item);
        return null;
    }

    /**
     * Добавить новый департамент в контейнер
     * @param parent - родительский департамент. Равен null для добавления корня
     * @param department - добавляемый департамент
     * @return - добавляемый департамент
     */
    public Department addDepartment(Department parent, Department department) {
        if (readOnlyMode) throw new IllegalArgumentException("The tree is in readonly mode.");
        Department parent_ = parent;
        if (parent_ == null) {
            parent_ = getRootDepartment();
            if (parent_ != null) throw new IllegalArgumentException("Root already exists.");
        }
        Item parentItem = null;
        if (parent_ != null) {
            parentItem = items.getItem(parent_.id);
            if (parentItem == null) throw new IllegalArgumentException("Parent item no found.");
            if (!items.areChildrenAllowed(parent_.id)) throw new IllegalArgumentException("Parent item cant have children.");
        }
        Item item = items.getItem(department.id);
        if (item == null) {
            item = items.addItem(department.id);
            items.setChildrenAllowed(department.id, true);
            Property property = item.getItemProperty(keyPropertyData);
            property.setValue(department);
        }
        items.setParent(department.id, parent_!=null?parent_.id:null);
        return department;
    }

    /**
     * Исключить заданный департамент из контейнера
     * @param department - департамент
     * @return - успех/неуспех
     */
    public boolean removeDepartment(Department department) {
        throw new UnsupportedOperationException("Method is not implemented yet.");
    }

    /**
     * Добавить нового работника в контейнер
     * @param department - родительский департамент
     * @param employee - работник
     * @return - работник
     */
    public Employee addEmployee(Department department, Employee employee) {
        if (readOnlyMode) throw new IllegalArgumentException("The tree is in readonly mode.");
        Item parentItem = items.getItem(department.id);
        if (parentItem == null) throw new IllegalArgumentException("Department node not found.");
        if (!items.areChildrenAllowed(department.id)) throw new IllegalArgumentException("Parent item cant have children.");
        Item item = items.getItem(employee.id);
        if (item == null) {
            item = items.addItem(employee.id);
            items.setChildrenAllowed(employee.id, false);
            Property property = item.getItemProperty(keyPropertyData);
            property.setValue(employee);
        }
        items.setParent(employee.id, department.id);
        return employee;
    }

    /**
     * Исключить работника из контейнера
     * @param department - из какого департамента
     * @param employee - работник
     * @return - успех/неуспех
     */
    public boolean removeEmployee(Department department, Employee employee) {
        throw new UnsupportedOperationException("Method is not implemented yet.");
    }

    /**
     * Найти департаменты по названию, в подветке от заданного департамента
     * @param s - название
     * @return - список департаментов
     */
    private Collection<Department> searchDepartmentsByTitle(Department fromDept, Collection<Department> resultList, String s) {
        if (fromDept.title.contains(s)) resultList.add(fromDept);
        for (Department subDep : getSubUnits(fromDept, Department.class)) searchDepartmentsByTitle(subDep, resultList, s);
        return resultList;
    }

    /**
     * Найти департаменты по названию
     * @param title - название
     * @return - список департаментов
     */
    public Collection<Department> searchDepartmentsByTitle(String title) {
        return searchDepartmentsByTitle(
                this.getRootDepartment(),
                new LinkedList<>(),
                title.toLowerCase().trim()
        );
    }

    /**
     * Получить юниты заданного класса под заданным департаментом
     * @param department - департамент
     * @param type - класс юнитов
     * @param <T> - один из наследников класса юнитов
     * @return - коллекция юнитов
     */
    public <T extends Unit> Collection<T> getSubUnits(Department department, Class<T> type) {
        Item item = items.getItem(department.id);
        if (item == null) throw new IllegalArgumentException("Department node not found.");
        Collection<T> result = new LinkedList<>();
        Collection subIds = items.getChildren(department.id);
        if (subIds == null) return result;
        for (Object subId : subIds) {
            Unit unit = getUnitFromItem(items.getItem(subId));
            if (type.isInstance(unit)) result.add((T) unit);
        }
        return result;
    }

    /**
     * Получить юниты любых классов под заданным департаментом
     * @param department - департамент
     * @return - коллекция юнитов
     */
    public Collection<Unit> getSubUnits(Department department) {
        return getSubUnits(department, Unit.class);
    }

    /**
     * Имеет ли заданный департамент дочерние юниты
     * @param department - департамент
     * @return - имеет/не имеет
     */
    public boolean hasSubUnits(Department department) {
        Item item = items.getItem(department.id);
        if (item == null) throw new IllegalArgumentException("Department node not found.");
        Collection subIds = items.getChildren(department.id);
        return (subIds!=null) && (!subIds.isEmpty());
    }

}
