/**
 * JS-компонента Паук
 */
// Define the namespaces
var companyStructure = companyStructure || {};
companyStructure.model = companyStructure.model || {};

//model entities
//элемент данных на входе, древовидный список
companyStructure.model.SourceNode = function () {
    this.id = "";
    this.unit = ""; //"DEPT" - отдел, "EMPL" - работник
    this.subNodes = []; //subnodes of SourceNode
    this.hasChilds = ""; //=1 for lazy loading subNodes
    //также могут быть определены: title, name, phone
};

//фабрика Пауков, на входе DOM-контейнер и ссылка на корневой элемент исходных данных типа SourceNode
companyStructure.createSpider = function (element, rootSourceData) {

    //счетчик для автогенерации идентификаторов, если в исходных данных не заданы
    var node_id_generator = 0;
    //объект визуальной модели, однозначно соответствует набору SVG-элементов и юниту исходных данных
    //создается только пока является видимым, затем уничтожается
    function Node(presenter, parentNode, sourceNode) {
        this.id = (sourceNode.id!=null)?sourceNode.id:node_id_generator++;
        this.sourceNode = sourceNode;
        this.expanded = false;
        this.x = 0;
        this.y = 0;
        this.parentNode = parentNode;
        this.presenter = presenter;
        this.subNodes = [];

        this.isDepartment = function() {return this.sourceNode.unit=="DEPT";};
        this.isEmployee = function() {return this.sourceNode.unit=="EMPL";};
    }

    //controller
    //на входе элемент DOM, который станет контейнером сцены
    //источником данных должна быть иерархическая структура объектов в формате companyStructure.model.SourceNode
    //подветки могут быть lazy-load, при их визуализации вызывается callback
    //для lazy-load проставить узлу hasChilds=true/1/'1' и определить код подзагрузки в onNeedToExpandAndGetChilds
    function Presenter(element) {
        this.element = element;
        //ссылка на корень источника данных, тип SourceNode
        this.rootSourceNode = null;
        //ссылка на корневой визуализируемый объект класса Node
        //объекты в этой иерархии создаются по мере вывода и уничтожаются сразу после скрытия визуального элеманта
        this.rootNode = null;
        //ссылка на визуализатор
        this.view = new View(element);
        //ссылка на раскидыватель юнитов по экрану
        this.diffuser = new Diffuser();
        //флаг - было ли проигрывание начальной анимации
        this.firstWasCalled = false;
        //хэш-таблица для быстрого поиска родительского элемента по дочернему, требуется при полном воссоздании графа
        //от заданного узла
        this.sourceChildIdToParentIdMap = [];
        //call-back для lazy-load, на входе companyStructure.model.SourceNode узла, имеющего hasChilds=true
        //подузлы следует добавлять в его атрибут subNodes
        //если подузлы добавлены, следует возвращать true, чтобы они успешно визуализировались
        this.onNeedToExpandAndGetChilds = function(sourceNode) {return false;};
        //call-back по событию выбора узла
        this.onNodeExpanded = function(sourceNode) { };

        //слушатель изменения размера окна
        var self = this;
        if (companyStructure.CONFIG.autoResize && window.addEventListener)
            window.addEventListener('resize', function(){ self.onResize(); });

        // ================ public methods =====================
        //установить корневой юнит источника данных
        this.setRootSourceNode = function (rootSourceNode) {
            if (this.rootSourceNode==rootSourceNode) return;
            this.rootSourceNode=rootSourceNode;
            this.rootNode = null;
            this.sourceChildIdToParentIdMap = [];
            this.fillChildIdToParentIdMap(this.rootSourceNode, null);
        };

        //свернуть все юниты
        this.collapseAll = function () {
            this.collapse(this.rootNode);
        };

        //развернуть заданный юнит (по идентификатору)
        this.expandNode = function (expandNodeId) {
            this.expand(this.getNodeById(expandNodeId));
        };

        //свернуть заданный юнит (по идентификатору)
        this.collapseNode = function (collapseNodeId) {
            this.collapse(this.getNodeById(collapseNodeId));
        };

        //визуализировать ветку к заданному юниту (по идентификатору)
        this.rebuildAndUpdate = function (expandNodeId) {
            this.firstWasCalled = this.firstWasCalled || (expandNodeId!=null);
            this.rootNode = this.buildRootNode();
            this.buildNodesInPath(this.rootNode, this.getPathToNode(expandNodeId));
            this.update();
            this.firstWasCalled = true;
        };

        //проверка, является ли заданный юнит видимым в текущий момент (по идентификатору)
        this.isNodeVisible = function(nodeId) {
            return this.getNodeById(nodeId)!=null;
        };

        //обработчик события изменения размера окна
        this.onResize = function() {
            if (this.element.offsetWidth != this.lastWidth || this.element.offsetHeight != this.lastHeight) {
                this.lastWidth = this.element.offsetWidth;
                this.lastHeight = this.element.offsetHeight;
                this.update();
            }
        };

        // ========================= private methods ======================================
        //построить модель корневого юнита
        this.buildRootNode = function () {
            if (!this.rootSourceNode) return new Node(this, null, {id: 1, unit: "DEPT", title: "No Data"});
            else return new Node(this, null, this.rootSourceNode);
        };

        //построить модель подветки к заданному юниту
        this.buildNodesInPath = function(node, path) {
            if (!path || (path.indexOf(node.id)==-1)) return;
            node.expanded = true;
            var self = this;
            node.sourceNode.subNodes.forEach(function(subSourceNode) {
                var subNode = new Node(self, node, subSourceNode);
                node.subNodes.push(subNode);
                self.buildNodesInPath(subNode, path);
            });
        };

        //заполнить хэш-таблицу ИД_юнита -> ИД_родительского_юнита
        this.fillChildIdToParentIdMap = function (sourceNode, parentSourceNodeId) {
            this.sourceChildIdToParentIdMap[sourceNode.id] = parentSourceNodeId;
            var self = this;
            if (sourceNode.subNodes) sourceNode.subNodes.forEach(function(subSourceNode) {
                self.fillChildIdToParentIdMap(subSourceNode, sourceNode.id);
            });
        };

        //вычислить путь к заданному юниту
        this.getPathToNode = function(nodeId) {
            if (!nodeId) return [];
            var path = [nodeId];
            var parentId = nodeId;
            do {
                parentId = this.sourceChildIdToParentIdMap[parentId];
                if (parentId) path.push(parentId);
            } while(parentId);
            return path;
        };

        //получить объект визуальной модели по идентификатору юнита
        this.getNodeById = function(nodeId) {
            //видимых нод немного, можно и перебором, операция дергается только снаружи и нечасто
            var getNodeByIdRecurs = function(nodeId, fromNode) {
                if (!fromNode) return null;
                if (fromNode.id==nodeId) return fromNode;
                for(var i = 0; i<fromNode.subNodes.length; i++ ) {
                    var r = getNodeByIdRecurs(nodeId, fromNode.subNodes[i]);
                    if (r) return r;
                }
                return null;
            };
            return getNodeByIdRecurs(nodeId, this.rootNode);
        };

        //развернуть визуальный юнит
        this.expand = function (node) {
            if (node.parentNode)
                node.parentNode.subNodes.forEach(function(subNode) {
                    if (subNode!=node && subNode.expanded) node.presenter.collapse(subNode);
                });
            if (!node.sourceNode) return false;
            if (!node.sourceNode.subNodes || !node.sourceNode.subNodes.length) {
                //lazy-loading
                if (node.sourceNode.hasChilds && !this.onNeedToExpandAndGetChilds(node.sourceNode)) return false;
            }
            if (!node.sourceNode.subNodes) return;
            for (var i = 0; i < node.sourceNode.subNodes.length; i++) {
                node.subNodes.push(new Node(this, node, node.sourceNode.subNodes[i]));
            }
            this.expandOnView(node, false);
            node.expanded = true;
            return true;
        };

        //свернуть визуальный юнит
        this.collapse = function (node) {
            node.subNodes.forEach(function(subNode) {
                if (subNode.expanded) subNode.presenter.collapse(subNode);
            });
            this.view.collapseNode(node);
            node.expanded = false;
            node.subNodes = [];
            this.zoomToNode(node.parentNode);
            return true;
        };

        //переключить развернуто-свернуто и наоборот для заданного визуального юнита
        this.switch = function(node) {
            if (node.isDepartment()) {
                if (!node.expanded) {
                    if (this.expand(node)) {
                        this.onNodeExpanded(node);
                        return true;
                    }
                    else return false;
                }
                else {
                    if (this.collapse(node)) {
                        this.onNodeExpanded(node.parentNode);
                        return true;
                    }
                    else return false;
                }
            }
            else return false;
        };

        //нарисовать подготовленную визуальную модель
        this.update = function() {
            this.view.initSvg();
            if (!this.rootNode) return;
            this.rootNode.x = this.view.center.x;
            this.rootNode.y = this.view.center.y;
            this.view.paintNode(this.rootNode, true, true, false, this.firstWasCalled);
            this.updateNode(this.rootNode);
        };

        //вывести ветку от заданного юнита
        this.updateNode = function(node) {
            if (node.expanded) {
                this.expandOnView(node, true);
                for (var i = 0; i < node.subNodes.length; i++) {
                    this.updateNode(node.subNodes[i]);
                }
            }
        };

        //сфокусировать на заданном юните и вывести его под-юниты
        this.expandOnView = function(node, isRepaint) {
            this.zoomToNode(node, isRepaint);
            this.diffuser.apply(node, node.subNodes, this.rootNode, this.view.winsize, node.parentNode!=null);
            for (var i = 0; i < node.subNodes.length; i++) {
                var subNode = node.subNodes[i];
                this.view.paintNode(subNode, false, subNode.isDepartment(), subNode.isEmployee(), isRepaint);
            }
            return true;
        };

        //сфокусировать на юните
        this.zoomToNode = function(node, isRepaint) {
            if (node!=this.rootNode && node) this.view.zoomToNode(node, isRepaint);
            else this.view.zoomToNode(null, isRepaint);
        };
    }

    // View - представление визуальной модели
    function View(element) {
        this.element = element;
        this.svg = null;
        this.svgScene = null;
        this.gLines = null;
        this.winsize = {width: 640, height: 480};
        this.center = {x: 320, y: 240};

        //пересоздать SVG-контейнер сцены
        this.initSvg = function() {
            if (this.svg) this.svg.remove();
            this.updateWinSize();
            this.svg = d3.select(this.element).append('svg')
                .attr('class', companyStructure.CONFIG.cssclass_SCENE)
                .attr('width', this.winsize.width)    // ширина
                .attr('height', this.winsize.height)  // высота
            ;
            this.svgScene = this.svg.append('g');
            this.gLines = this.svgScene.append('g');
        };

        //пересчитать размер сцены
        this.updateWinSize = function() {
            var width = companyStructure.CONFIG.width?companyStructure.CONFIG.width:
                (this.element.offsetWidth-companyStructure.CONFIG.margin*2);
            var height = companyStructure.CONFIG.height?companyStructure.CONFIG.height:
                (this.element.offsetHeight-companyStructure.CONFIG.margin*2);
            this.winsize.width = width > companyStructure.CONFIG.minWidth?width:companyStructure.CONFIG.minWidth;
            this.winsize.height = height > companyStructure.CONFIG.minHeight?height:companyStructure.CONFIG.minHeight;
            this.center.x = (this.winsize.width)/2;
            this.center.y = (this.winsize.height)/2;
        };

        //вывести юнит
        this.paintNode = function (node, isRoot, isDept, isEmployee, isRepaint) {
            if (isDept && !isRoot) {
                //вывести ребро графа
                var line = this.gLines.append('line')
                    .datum(node)
                    .attr("parent_node_id", node.parentNode.id)
                    .attr('class', companyStructure.CONFIG.cssclass_EDGE)
                    .attr('x1', node.parentNode.x)
                    .attr('y1', node.parentNode.y)
                    .attr('x2', node.parentNode.x)
                    .attr('y2', node.parentNode.y)
                    ;
                (isRepaint?line:line
                    .attr("stroke-opacity", 0)
                    .transition()
                    .duration(companyStructure.CONFIG.svg_animation_NODE_EXPAND_DURATION))
                    .attr("stroke-opacity", 1)
                    .attr('x2', node.x)
                    .attr('y2', node.y)
                ;
            }
            //вывести узел графа
            var circle = this.svgScene.append('circle')
                .datum(node)
                .attr("parent_node_id", isRoot?"":node.parentNode.id)
                .attr('class',
                    isRoot?companyStructure.CONFIG.cssclass_ROOT
                    :isDept?companyStructure.CONFIG.cssclass_DEPARTMENT
                    :companyStructure.CONFIG.cssclass_EMPLOYEE)
                .attr('cx', node.x)
                .attr('cy', node.y)
                .on('click', function (node) {
                   return node.presenter.switch(node);
                })
                .attr("r", isRoot?companyStructure.CONFIG.svg_animation_FIRST_BIG_BIG_ROOT_RADIUS:0)
                ;
            (isRepaint?circle:circle
                .attr('cx', node.parentNode?node.parentNode.x:node.x)
                .attr('cy', node.parentNode?node.parentNode.y:node.y)
                .attr("fill-opacity", 0)
                .attr("stroke-opacity", 0)
                .transition()
                .duration(companyStructure.CONFIG.svg_animation_NODE_EXPAND_DURATION))
                .attr('cx', node.x)
                .attr('cy', node.y)
                .attr("fill-opacity", 1)
                .attr("stroke-opacity", 1)
                .attr("r",  isRoot?companyStructure.CONFIG.svg_CIRCLE_R_ROOT
                           :isDept?companyStructure.CONFIG.svg_CIRCLE_R_DEPARTMENT
                           :companyStructure.CONFIG.svg_CIRCLE_R_EMPLOYEE)
            ;
            if (isDept) {
                //вывести надпись к узлу графа
                circle.append("title")
                    //TO-DO вынести тексты в CONFIG
                    .text((node.sourceNode.subNodes && node.sourceNode.subNodes.length)?
                                            "Click to show/hide "+node.sourceNode.subNodes.length+" sub-units"
                        :(node.sourceNode.hasChilds?   "Click to show/hide sub-units"
                        :                   "No sub-units to show"
                    ));
            }
            if (isDept) {
                var text = this.svgScene.append('text')
                    .datum(node)
                    .attr("parent_node_id", isRoot?"":node.parentNode.id)
                    .attr('class', companyStructure.CONFIG.cssclass_TITLE)
                    .text(node.sourceNode.title)
                    .on('click', function (node) {
                        return node.presenter.switch(node);
                    });
                var deltaY = 5 +
                        (isRoot?companyStructure.CONFIG.svg_CIRCLE_R_ROOT:companyStructure.CONFIG.svg_CIRCLE_R_DEPARTMENT);
                (isRepaint?text:text
                    .attr("fill-opacity", -0.5)
                    .attr('x', node.parentNode?node.parentNode.x:node.x)
                    .attr('y', node.parentNode?node.parentNode.y - deltaY:(node.y - deltaY))
                    .attr("transform", "translate(" +
                        (node.parentNode?node.parentNode.x:node.x) + "," +
                        (node.parentNode?node.parentNode.y - deltaY:(node.y - deltaY)) + ")" +
                        "scale(0)")
                    .transition()
                    .duration(companyStructure.CONFIG.svg_animation_NODE_EXPAND_DURATION))
                    .attr("fill-opacity", 1)
                    .attr('x', node.x)
                    .attr('y', node.y - deltaY)
                    .attr("transform", "")
                ;
            }
            if (isEmployee) {
                //добавить обработчик мыши для вывода всплывающего окна с информацией
                this.svgScene.selectAll("circle[parent_node_id='"+node.parentNode.id+"']")
                    .on("mouseover", function(node) {
                        var tooltip = node.presenter.view.tooltip;
                        tooltip.interrupt().transition();
                        tooltip.text("");
                        tooltip.append("span").text("Name: "+ node.sourceNode.name);
                        tooltip.append("br");
                        tooltip.append("span").text(node.parentNode.sourceNode.title);
                        tooltip.append("br");
                        tooltip.append("span").text("Phone: " + node.sourceNode.phone);
                        tooltip
                            .style("top", (d3.event.pageY-50) + "px")
                            .style("left",(d3.event.pageX+20) + "px")
                            .style("visibility", "visible")
                            .style("opacity", 0.0)
                            .transition()
                            .delay(150)
                            .duration(companyStructure.CONFIG.svg_animation_TOOLTIP_EXPAND_DURATION)
                            .style("opacity", 1.0)
                    })
                    .on("mouseout", function(node){
                        var tooltip = node.presenter.view.tooltip;
                        tooltip.interrupt().transition();
                        tooltip
                            .style("opacity", 1.0)
                            .transition()
                            .duration(companyStructure.CONFIG.svg_animation_TOOLTIP_HIDE_DURATION)
                            .style("opacity", 0.0)
                            .transition()
                            .delay(companyStructure.CONFIG.svg_animation_TOOLTIP_HIDE_DURATION)
                            .style("top", "0")
                            .style("left", "0")
                            ;
                    });
            }
            return true;
        };

        //свернуть заданный юнит
        this.collapseNode = function (node) {
            this.svgScene.selectAll("circle[parent_node_id='"+node.id+"']")
                .transition()
                .duration(companyStructure.CONFIG.svg_animation_NODE_COLLAPSE_DURATION)
                .attr("r", 0)
                .remove();
            this.svgScene.selectAll("text[parent_node_id='"+node.id+"']")
                .transition()
                .duration(companyStructure.CONFIG.svg_animation_NODE_COLLAPSE_DURATION)
                .attr("fill-opacity", 0)
                .remove();
            this.svgScene.selectAll("line[parent_node_id='"+node.id+"']")
                .transition()
                .duration(companyStructure.CONFIG.svg_animation_NODE_COLLAPSE_DURATION)
                .attr("stroke-opacity", 0)
                .attr('x2', node.x)
                .attr('y2', node.y)
                .remove();
        };

        //сфокусироваться на заданном юните
        this.zoomToNode = function(node, isRepaint) {
            if (node!=null) {
                (isRepaint?this.svgScene:this.svgScene
                    .transition()
                    .duration(companyStructure.CONFIG.svg_animation_NODE_ZOOM_DURATION))
                    .attr("transform", "translate(" +
                        (this.center.x - node.x * companyStructure.CONFIG.svg_ZOOM) + "," +
                        (this.center.y - node.y * companyStructure.CONFIG.svg_ZOOM) + ")" +
                        "scale(" + companyStructure.CONFIG.svg_ZOOM + ")");
            }
            else (isRepaint?this.svgScene:this.svgScene
                .transition()
                .duration(companyStructure.CONFIG.svg_animation_NODE_ZOOM_DURATION))
                .attr("transform", "");
        };

        //создать всплывающее окно для того, чтобы затем его использовать
        this.tooltip = d3.select("body")
            .append("div")
            .attr('class', companyStructure.CONFIG.cssclass_TOOLTIP)
            .style("position", "absolute")
            .style("z-index", "10")
            .style("opacity", "0")
            .style("top", "0px")
            .style("left", "0px");
    }

    //Распределитель точек по видимой части сцены
    //реализация по средней площади
    //определяет площадь на один юнит, разбивает всю сцену на квадраты этой площади
    //расставляет юниты в центрах этих квадратов
    //если не все поместились, то оставшиеся расставляет между квадратами
    //следит, чтобы юниты не пересеклись с родительским и корневыми юнитами
    function Diffuser() {
        this.apply = function(parentNode, subNodes, rootNode, winsize, zoomMode) {
            //определяет, неприлично ли близко находится точка к другому юниту
            var pointInNode = function(x, y, node) {
                return ((Math.abs(node.x - x) < 3*companyStructure.CONFIG.svg_CIRCLE_R_ROOT) &&
                        (Math.abs(node.y - y) < 3*companyStructure.CONFIG.svg_CIRCLE_R_ROOT));
            };
            if (!subNodes || !subNodes.length) return;
            //определяем размеры видимой части сцены
            var winHeight = zoomMode?(winsize.height/companyStructure.CONFIG.svg_ZOOM):winsize.height;
            var winWidth = zoomMode?(winsize.width/companyStructure.CONFIG.svg_ZOOM):winsize.width;
            var offsetX = zoomMode?(parentNode.x - (winWidth/companyStructure.CONFIG.svg_ZOOM)):0;
            var offsetY = zoomMode?(parentNode.y - (winHeight/companyStructure.CONFIG.svg_ZOOM)):0;
            //определяем размер стороны квадрата
            var side = Math.sqrt((winWidth * winHeight)/subNodes.length);
            //определяем сколько рядов и колонок образуют эти квадраты в видимой части сцены
            var rows = Math.round(winHeight / side);
            var cols = Math.round(winWidth / side);
            var i = 0;
            //распределение юнитов по центрам квадратов, слева направо, сверху вниз
            var curRow = 0;
            var curCol = 0;
            while (i<subNodes.length) {
                //сдвиг на следующую строку если съехали за пределы колонок
                if (curCol>=cols) {
                    curRow++;
                    curCol = 0;
                }
                if (curRow>=rows) break;
                //вычисление середины очередного квадрата
                var x = offsetX + (curCol*side) + (side/2);
                var y = offsetY + (curRow*side) + (side/2);
                // если рядом родительские, то пропустим этот квадрат
                if (!pointInNode(x, y, parentNode) && !pointInNode(x, y, rootNode)) {
                    //запомним координаты в юните
                    subNodes[i].x = x;
                    subNodes[i].y = y;
                    i++;
                }
                //к следующему квадрату
                curCol++;
            }
            //распределение оставшихся юнитов между квадратами, слева направо, сверху вниз
            curRow = 0;
            curCol = 0;
            while (i<subNodes.length) {
                //сдвиг на следующую строку если съехали за пределы колонок
                if (curCol+1>=cols) {
                    curRow++;
                    curCol = 0;
                }
                if (curRow>=rows) break;
                //вычисление нижней правой позиции квадрата
                x = offsetX + (curCol+1)*side;
                y = offsetY + (curRow+1)*side;
                // если рядом родительский, то пропустим квадрат
                if (!pointInNode(x, y, parentNode) && !pointInNode(x, y, rootNode)) {
                    //запомним координаты в юните
                    subNodes[i].x = x;
                    subNodes[i].y = y;
                    i++;
                }
                //к следующему квадрату
                curCol++;
            }
        }
    }

    //предварительная обработка исходной модели
    var normalizeSourceData = function(node) {
          if (node) {
              if (!node.unit) node.unit = (node.subNodes && node.subNodes.length)?"DEPT":"EMPL";
              if (node.subNodes && node.subNodes.length) {
                  node.subNodes.forEach(function(subNode) {
                      normalizeSourceData(subNode);
                  });
              }
          }
    };

    var presenter = new Presenter(element);
    if (rootSourceData) {
        normalizeSourceData(rootSourceData);
        presenter.setRootSourceNode(rootSourceData);
    }
    return presenter;
};


//настройки
companyStructure.CONFIG = companyStructure.CONFIG || {
        cssclass_SCENE : "spider-scene",
        cssclass_ROOT : "spider-root",
        cssclass_DEPARTMENT : "spider-department",
        cssclass_EMPLOYEE : "spider-employee",
        cssclass_TITLE : "spider-title",
        cssclass_EDGE : "spider-edge",
        cssclass_TOOLTIP : "spider-tooltip",
        svg_CIRCLE_R_ROOT : 10,
        svg_CIRCLE_R_DEPARTMENT : 8,
        svg_CIRCLE_R_EMPLOYEE : 4,
        svg_ZOOM : 2,
        svg_animation_FIRST_BIG_BIG_ROOT_RADIUS : 400,
        svg_animation_NODE_EXPAND_DURATION : 1000,
        svg_animation_NODE_COLLAPSE_DURATION : 1000,
        svg_animation_NODE_ZOOM_DURATION : 750,
        svg_animation_TOOLTIP_EXPAND_DURATION : 400,
        svg_animation_TOOLTIP_HIDE_DURATION : 600,
        autoResize: true,
        width: null,
        height: null,
        minWidth: 100,
        minHeight: 100,
        margin: 10
    };